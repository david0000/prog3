const request = require('request');
const scrapeIt = require('scrape-it');
const jsonfile = require('jsonfile');
const iconv = require('iconv-lite');



var id = 1;
scraperloop(id);
var obj = { "films": [] };


var data = {
    // Fetch the articles
    articles: {
        listItem: "div.shortstory",
        data: {
            title: ".shortstorytitle .zagolovki a",
            date: ".shortimg a:nth-child(5)",
            contry: {
                selector: ".shortimg",// cherez inch gdni uzact
                country: 'a', //inch@ qezi ta,
                convert: function (x) {
                    var regex = /Страна: (.+)Жанр:/i;
                    if (x.match(regex)) {
                        var found = x.match(regex);
                        var res = found[1].split(", ")
                        return res;
                    } else {
                        return "";
                    }
                }
                
            },
            jhanr: {
                selector: ".shortimg",// cherez inch gdni uzact
                jhanr: 'a', //inch@ qezi ta,
                convert: function (x) {
                    var myRegexp = /Жанр: (.+)Качество:/i;
                    if (x.match(myRegexp)) {
                        var found = x.match(myRegexp);
                        var res = found[1].split(", ")
                        return res;
                    } else {
                        return "";
                    }
                }
            },
            erkar: {
                selector: ".shortimg",// cherez inch gdni uzact
                erkar: 'a', //inch@ qezi ta,
                convert: function (x) {
                    var myRegexp = /Продолжительность: (.+)Премьера/i;
                    if (x.match(myRegexp)) {
                        var found = x.match(myRegexp);
                        var res = found[1].split(":");
                        res=res[0]*3600+res[1]*60+res[2];
                        return res;
                    } else {
                        return "";
                    }
                }
            },
        }
    }
}

function callback(error, response, body) {
    if (!error && response.statusCode == 200) {


        body = iconv.decode(body, 'win1251');
        page = scrapeIt.scrapeHTML(body, data);
        obj.films = obj.films.concat(page.articles);


    }
}

function scraperloop(i) {
    setTimeout(function () {

        //var url = 'http://kinogo.cc/page/' + i + '/';
        request({
            uri: 'http://kinogo.cc/page/' + i + '/',
            encoding: null
        }, callback);
        if (i <= 1)
            scraperloop(++i);
        else {
            jsonfile.writeFile('info.json', obj, { spaces: 2 }, function (err) {
                console.error(err || 'Ashxatav')
            });
        }
    }, 2000)
}